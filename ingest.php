<?php

namespace Proclaim;
spl_autoload_register();

use Proclaim\Core\Api\PostAPI;
use Proclaim\Core\Database;
use Proclaim\Core\DatabaseConfiguration;
use Proclaim\Core\MySQLAdapter;
use Proclaim\Core\Models\PostData;
use Proclaim\Core\Utilities;

define( "PROCLAIM_ROOT", Utilities::add_slash( $_SERVER["DOCUMENT_ROOT"]) );

// Set up database
$db_config = new DatabaseConfiguration( "proclaim/config/proclaim-db.json" );
$db_connection = new MySQLAdapter( $db_config );
$db = new Database( $db_config, $db_connection);
$posts = new PostData( $db );
$allposts = $posts->getAllPosts();
$api = new PostAPI( $posts );
$api->printPosts();

//$query = utf8_encode( "SELECT * FROM proclaim_posts" );
//$results = $database->connection()->query( $query );
var_dump( $allposts );
foreach ( $allposts as $post ) {
    echo $post["post_content"];
}