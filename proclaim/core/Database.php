<?php

 namespace Proclaim\Core;

 interface DatabaseInterface
 {
     public function __construct(DatabaseConfiguration $config, DatabaseConnectionAdapter $dsn );
     public function connection(): \PDO;
 }

 class Database implements DatabaseInterface
 {
     protected $connection;
     protected $configuration;
     protected $dsn;

     public function __construct(DatabaseConfiguration $config, DatabaseConnectionAdapter $dsn )
     {
        $this->configuration = $config;
        $this->dsn = $dsn;
     }

     public function connect()
     {
         $this->connection = new \PDO(
             $this->dsn->getDsn(),
             $this->configuration->getUsername(),
             $this->configuration->getPassword(),
             $this->dsn->getOptions()
         );
     }

     /**
      * @return \PDO
      * @throws \Exception
      */
     public function connection(): \PDO
     {
         if( isset( $this->connection ) ) return $this->connection;
         throw new \Exception("No connection found");
     }

 }