<?php

namespace Proclaim\Core;

class Utilities
{
    /**
     * Converts a JSON file to a stdClass
     * @param string $path
     * @param bool|null $add_root
     * @return \stdClass
     */
    public static function json_decode_file(string $path, boolean $add_root = null ): \stdClass
    {
        if( $add_root )
        {
            $path = utf8_encode( PROCLAIM_ROOT.$path );
        }
        $contents = file_get_contents( $path );
        $json = json_decode( $contents );

        return $json;
    }

    /**
     * Adds a trailing slash to a path if it is missing
     * @param string $path
     * @return string
     */
    public static function add_slash( string $path ): string
    {
        if( $path[mb_strlen( $path ) - 1] !== "/" )
        {
            $path = utf8_encode( $path."/" );
        }
        return $path;
    }
}