<?php

namespace Proclaim\Core\Models;
use Proclaim\Core\Database;

class PostData
{
    private $db;

    public function __construct(Database $db )
    {
        $this->db = $db;
    }

    public function getAllPosts(): array
    {
        $this->db->connect();
        $stmt = null;
        try {
            $stmt = $this->db->connection()->prepare(
                utf8_encode( "SELECT * FROM proclaim_posts" ) );
            $stmt->execute();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getByID( int $id ): array
    {
        $this->db->connect();
        $stmt = null;1
        try {
            $stmt = $this->db->connection()->prepare(
                'SELECT * FROM proclaim_posts WHERE id = :id' );
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}