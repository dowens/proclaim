<?php

namespace Proclaim\Core;

interface DatabaseConnectionAdapter
{
    public function __construct(DatabaseConfiguration $config);
    public function getDsn();
    public function getOptions();
}

class MySQLAdapter implements DatabaseConnectionAdapter
{
    /**
     * @var DatabaseConfiguration
     */
    private $configuration;

    /**
     * @var array $opt
     */
    private $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,
    ];

    /**
     * MySQLConnection constructor.
     * @param DatabaseConfiguration $config
     */
    public function __construct(DatabaseConfiguration $config)
    {
        $this->configuration = $config;
    }

    public function getDsn(): string
    {
        $dsn = "mysql:";
        if( $host = $this->configuration->getHost() ) {
            $dsn .= "host=".$host.";";
        }
        if( $charset = $this->configuration->getCharset() ) {
            $dsn .= "charset=".$charset.";";
        }
        if( $port = $this->configuration->getPort() ) {
            $dsn .= "port=".$port.";";
        }
        if( $collation = $this->configuration->getCollation() ) {
            $dsn .= "collation=".$collation.";";
        }
        if( $database_name = $this->configuration->getDatabaseName() ) {
            $dsn .= "dbname=".$database_name.";";
        }
        return $dsn;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

}