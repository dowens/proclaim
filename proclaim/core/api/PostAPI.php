<?php

namespace Proclaim\Core\Api;

use Proclaim\Core\Models\PostData;

class PostAPI
{
    protected $posts;

    public function __construct(PostData $posts )
    {
        $this->posts = $posts;
    }

    public function printPosts()
    {
        $json = json_encode( $this->posts->getAllPosts() );
        echo "<script> console.log($json) </script>";
    }
}