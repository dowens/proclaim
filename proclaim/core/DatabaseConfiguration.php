<?php

namespace Proclaim\Core;

class DatabaseConfiguration
{
    /**
     * @var string
     */
    private $host;

    /**`
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $user_name;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $charset;

    /**
     * @var string
     */
    private $database_name;

    /**
     * @var string
     */
    private $collation;

    public function __construct( string $path )
    {
        $this->setViaJSON( $path );
    }

    public function setHost( string $host )
    {
        $this->host = $host;
    }

    public function setPort( int $port )
    {
        $this->port = $port;
    }

    public function setUsername( string $username )
    {
        $this->username = $username;
    }

    public function setPassword( string $password ){
        $this->password = $password;
    }

    public function setDatabaseName( string $database_name )
    {
        $this->database_name = $database_name;
    }

    public function setCharset( string $charset )
    {
        $this->charset = $charset;
    }

    public function setCollation( string $collation )
    {
        $this->collation = $collation;
    }

    public function setViaJSON( string $path ): self
    {
        $config = Utilities::json_decode_file( $path );
        if( isset($config->host) ) {
            $this->host = $config->host;
        }
        if( isset($config->port) ) {
            $this->port = $config->port;
        }
        if( isset($config->user_name) ) {
            $this->user_name = $config->user_name;
        }
        if( isset($config->password) ) {
            $this->password = $config->password;
        }
        if( isset($config->database_name) ) {
            $this->database_name = $config->database_name;
        }
        if( isset($config->charset) ) {
            $this->charset = $config->charset;
        }
        if( isset($config->collation) ) {
            $this->collation = $config->collation;
        }

        return $this;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPort(): int
    {
        return $this->port;
    }

    public function getUsername(): string
    {
        return $this->user_name;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getCharset(): string
    {
        return $this->charset;
    }

    public function getDatabaseName(): string
    {
        return $this->database_name;
    }

    public function getCollation(): string
    {
        return $this->collation;
    }
}

